public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] closedTiles;

	public Board(){
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.closedTiles = new boolean[12];
	}
	public String toString(){
		String s = "";
		int count = 1;
		for (boolean i : this.closedTiles){
			if (i){
				s = s + "X" + ", ";
			} else {
				s = s + count + ", ";
			}
			count++;
		}
		return s;
	}
	public boolean playATurn(){
		this.dieOne.roll();
		this.dieTwo.roll();
		System.out.println(this.dieOne);
		System.out.println(this.dieTwo);
		int sum = dieOne.getPips() + dieTwo.getPips();
		if(this.closedTiles[sum-1]==true){
			System.out.println("Place is shut");
			return true;	
		} else {
			this.closedTiles[sum-1]=true;
			return false; 
			}
	}
}