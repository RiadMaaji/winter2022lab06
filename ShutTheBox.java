public class ShutTheBox{
	public static void main(String[] args){
	System.out.println("Welcome to Shut the box!");
	Board box = new Board();
	Boolean gameOver = false;

	while(!gameOver){
	System.out.println("Player 1's turn");
	System.out.println(box);
	
	if (box.playATurn()){
		System.out.println("Player 2 wins");
		gameOver = true;
		System.out.println(box);
		break;
	} else {
		System.out.println("Player 2's turn");
		System.out.println(box);
	}
	if (box.playATurn()){
		System.out.println("Player 1 wins");
		gameOver = true;
	}
	}
}
}